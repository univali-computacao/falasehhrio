angular.module('starter.services', [])

.factory('Chats', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var chats = [{
    id: 0,
    name: 'André',
    lastText: 'Carro estaciona em lugar proibido',
    face: 'https://pbs.twimg.com/profile_images/514549811765211136/9SgAuHeY.png',
    status: 1
  }, {
    id: 1,
    name: 'Manoel',
    lastText: 'Acidente de carro',
    face: 'https://avatars3.githubusercontent.com/u/11214?v=3&s=460',
    status: 2
  }, {
    id: 2,
    name: 'Lucas',
    lastText: 'Pessoa ameaça idoso',
    face: 'https://pbs.twimg.com/profile_images/479090794058379264/84TKj_qa.jpeg',
    status: 1
  }, {
    id: 3,
    name: 'Derique',
    lastText: 'Briga de comerciantes',
    face: 'https://pbs.twimg.com/profile_images/598205061232103424/3j5HUXMY.png',
    status: 3
  }, {
    id: 4,
    name: 'João',
    lastText: 'Mulheres brigando por homem',
    face: 'https://pbs.twimg.com/profile_images/578237281384841216/R3ae1n61.png',
    status: 3
  }];
  
  return {
    all: function() {
      return chats;
    },
    remove: function(chat) {
      chats.splice(chats.indexOf(chat), 1);
    },
    get: function(chatId) {
      for (var i = 0; i < chats.length; i++) {
        if (chats[i].id === parseInt(chatId)) {
          return chats[i];
        }
      }
      return null;
    }
  };
});
